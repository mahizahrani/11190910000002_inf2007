package id.mahizahrani.PembayaranBuku.controller;

import com.google.gson.Gson;
import id.mahizahrani.PembayaranBuku.controller.Menu;
import id.mahizahrani.PembayaranBuku.model.Pembayaran;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import static java.lang.System.in;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class PembayaranController {

    private static final String FILE = "D:\\parkir.json";
    private Pembayaran pembayaran;
    private final Scanner in;
    private String nama, judulBuku;
    private BigDecimal harga;
    private int jumlahBeli, jenisBuku;
    private int pilihan;
    private LocalDateTime waktuBeli;
    private final DateTimeFormatter dateTimeFormat;

    public PembayaranController() {
        in = new Scanner(System.in);
        waktuBeli=LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }

    public void setPembayaranBuku() {
        System.out.print("Masukkan Nama pembeli : ");
        nama = in.next();
        System.out.println("Jenis Buku :");
        System.out.println("1. Komik\t 2. Novel\t 3.Buku Pelajaran");
        System.out.print("Masukkan Jenis buku : ");
        while (!in.hasNextInt()) {
            String input = in.next();
            System.out.printf("\"%s\" jangan masukkan huruf.\n", input);
            System.out.print("Masukkan Jenis Buku : ");
        }
        jenisBuku = in.nextInt();
        System.out.println("masukkan judul buku");
        judulBuku = in.next();
        System.out.println("Masukkan jumlah beli");
        jumlahBeli = in.nextInt();

        pembayaran = new Pembayaran();
        waktuBeli =LocalDateTime.now();
        pembayaran.setWaktuBeli(waktuBeli);
        if (jumlahBeli > 0) {
            harga = new BigDecimal(jumlahBeli);
            if (jenisBuku == 1) {
                harga = harga.multiply(new BigDecimal(50000));
            } else if (jenisBuku == 2) {
                harga = harga.multiply(new BigDecimal(100000));

            } else if (jenisBuku == 3) {
                harga = harga.multiply(new BigDecimal(200000));
            }
        }
        
        System.out.println("Jumlah harga : " + harga);
        
        String formatWaktuMasuk = waktuBeli.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatWaktuMasuk);
        pembayaran.setJumlahBeli(jumlahBeli);
        
        pembayaran.setHarga(harga);
        pembayaran.setNama(nama.toUpperCase());
        pembayaran.setJenisBuku(jenisBuku);
        pembayaran.setJudulBuku(judulBuku);
        pembayaran.setKeluar(true);

        setWritePembayaran(FILE, pembayaran);

        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPembayaranBuku();
            System.out.println("");
        }
    }

    public void setWritePembayaran(String file, Pembayaran pembayaran) {
        Gson gson = new Gson();

        List<Pembayaran> pembayarans = getReadPembayaran(file);
        pembayarans.remove(pembayaran);
        pembayarans.add(pembayaran);

        String json = gson.toJson(pembayarans);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(PembayaranController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Pembayaran> getReadPembayaran(String file) {
        List<Pembayaran> pembayarans = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                Pembayaran[] ps = gson.fromJson(line, Pembayaran[].class);
                pembayarans.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PembayaranController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PembayaranController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pembayarans;

    }

    public void getDataPembayaran() {
        List<Pembayaran> pembayarans = getReadPembayaran(FILE);
        Predicate<Pembayaran> isKeluar = e -> e.isKeluar() == true;
        Predicate<Pembayaran> isDate = e -> e.getWaktuBeli().toLocalTime().equals(LocalDate.now());

        List<Pembayaran> pResults = pembayarans.stream().filter(isKeluar).collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(Pembayaran::getHarga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Nama pembeli \tJenis buku \tJudul buku \tHarga\tjumlah beli\twaktu beli");
        System.out.println("------------ \t---------- \t---------- \t-----\t-----------\t----------");
        pResults.forEach((b) -> {
            if (b.getJenisBuku()==1){
            System.out.println(b.getNama() + "\t\t" + "Komik" + "\t\t" + b.getJudulBuku() + "\t\t" + b.getHarga() + "\t" + b.getJumlahBeli() + "\t\t" + b.getWaktuBeli().format(dateTimeFormat));
            }else if(b.getJenisBuku()==2) {
            System.out.println(b.getNama() + "\t\t" + "Novel" + "\t\t" + b.getJudulBuku() + "\t\t" + b.getHarga() + "\t" + b.getJumlahBeli() + "\t\t" + b.getWaktuBeli().format(dateTimeFormat));    
            }else if(b.getJenisBuku()==3){
                System.out.println(b.getNama() + "\t\t" + "Buku Pelajaran" + "\t" + b.getJudulBuku() + "\t" + b.getHarga() + "\t" + b.getJumlahBeli() + "\t\t" + b.getWaktuBeli().format(dateTimeFormat));
            
            }
            });
        System.out.println("------------ \t---------- \t---------- \t-----\t-----------\t----------");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataPembayaran();
        }
    }

}
