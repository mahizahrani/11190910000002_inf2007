package id.mahizahrani.PembayaranBuku.controller;

import id.mahizahrani.PembayaranBuku.model.Info;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Menu {

    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");

        System.out.println("1.Pembelian Buku");
        System.out.println("2.Laporan Keuangan");
        System.out.println("3.Keluar aplikasi");

        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" jangan masukkan huruf!.\n", input);
                System.out.print("Pilih Menu (1/2/3) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        PembayaranController lov = new PembayaranController();
        switch (noMenu) {
            case 1:
                lov.setPembayaranBuku();
                break;
            case 2:
                lov.getDataPembayaran();
                break;
            case 3:
                System.out.println("Sampai jumpa :)");
                System.exit(0);
                break;

        }
    }

}
