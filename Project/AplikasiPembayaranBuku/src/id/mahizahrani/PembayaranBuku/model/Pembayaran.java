package id.mahizahrani.PembayaranBuku.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author ASUS
 */
public class Pembayaran {
  private static final long serialVersionUID = -6756463875294313469L;

    private String nama,judulBuku;
    private int jumlahBeli,jenisBuku;
    private BigDecimal harga;
    private LocalDateTime waktuBeli;
    private boolean keluar =false;
    public Pembayaran() {
    
}
    public Pembayaran(String nama, int jenisBuku,String judulBuku, int jumlahBeli, BigDecimal harga,LocalDateTime waktuBeli) {
        this.nama = nama;
        this.jenisBuku = jenisBuku;
        this.judulBuku = judulBuku;
        this.jumlahBeli = jumlahBeli;
        this.harga = harga;
        this.waktuBeli = waktuBeli;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getJenisBuku() {
        return jenisBuku;
    }

    public void setJenisBuku(int jenisBuku) {
        this.jenisBuku = jenisBuku;
    }

    public String getJudulBuku() {
        return judulBuku;
    }

    public void setJudulBuku(String judulBuku) {
        this.judulBuku = judulBuku;
    }

    public int getJumlahBeli() {
        return jumlahBeli;
    }

    public void setJumlahBeli(int jumlahBeli) {
        this.jumlahBeli = jumlahBeli;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public LocalDateTime getWaktuBeli() {
        return waktuBeli;
    }

    public void setWaktuBeli(LocalDateTime waktuBeli) {
        this.waktuBeli = waktuBeli;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    
   @Override
    public String toString() {
        return "Pembayaran{" + "nama=" + nama + ", jenisBuku=" + jenisBuku + ", judulBuku=" + judulBuku + "jumlah beli=" +jumlahBeli +", harga=" + harga+"waktuBeli = " +waktuBeli +",keluar =" + keluar +'}';

    }
} 
    

