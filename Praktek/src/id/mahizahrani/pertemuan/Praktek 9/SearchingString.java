package id.mahizahrani.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class SearchingString {
    public int SequentialString(String L[], int n, String x) {
        int i = 0;
        while ((i < n - 1) && (!x.equals(L[i]))) {
            i = i + 1;
        }
        if (x.equals(L[i])) {
            return i;
        } else {
            return -1;
        }
    }

    public int BinarySearch(String L[], int n, String x) {
        int i = 0;
        int j = n - 1;
        int k = 0;
        boolean ketemu = false;

        while ((i <= j) && (!ketemu)) {
            k = (i + j) / 2;
            if (x.equals(L[k])) {
                ketemu = true;
            } else {
                if (x.compareTo(L[k]) > 0) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String L[] = {"ali", "indah","mawar", "Melati"};
        int n;
        n = L.length;
        String x;
        System.out.println("Masukan nama : ");
        x = in.next();
        SearchingString app = new SearchingString();
        System.out.println("===SEQUENTIAL SEARCH===");
        System.out.println("Indeks ke " + app.SequentialString(L, n, x));
        System.out.println("===BINARY SEARCH===");
        System.out.println("Indeks ke " + app.BinarySearch(L, n, x));
    }
}


