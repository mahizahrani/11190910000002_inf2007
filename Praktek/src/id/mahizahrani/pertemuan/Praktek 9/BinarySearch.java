package id.mahizahrani.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class BinarySearch {

    private int i, j, k;
    private boolean ketemu;

    public int getBinarySearch(int L[], int n, int X) {
        i = 0;
        j = n;
        boolean ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == X) {
                ketemu = true;
            } else {
                if (L[k] < X) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;

        } else {
            return -1;
        }
    }
    public static void main(String[] args) {
        int[] L = {13,14,15,16,21,76};
        int X, n = 6;

        BinarySearch app = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan X = ");
        X = in.nextInt();

        System.out.println(" index= " + app.getBinarySearch(L, n, X));
    }
}
   
