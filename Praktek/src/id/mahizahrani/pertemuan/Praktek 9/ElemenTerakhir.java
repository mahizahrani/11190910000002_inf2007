package id.mahizahrani.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ElemenTerakhir {
   public int getIndeks(int[] L, int n, int x) {
        int i = n - 1;
        Boolean ketemu = false;
        while ((i > -1) && (i <= n - 1) && (!ketemu)) {
            if (i <= n - 1) {
                System.out.println("Posisi ke- " + i + " isinya " + L[i]);
            }
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i - 1;
                        }
        }
        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }
 
    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
        int n = 6;
        Scanner in = new Scanner (System.in);
        ElemenTerakhir app = new ElemenTerakhir ();
        int X = in.nextInt();
        System.out.println("elemen "+ app.getIndeks(L, n, X));
    }
}