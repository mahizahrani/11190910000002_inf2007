package id.mahizahrani.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class HurufVokal {

    public static void main(String[] args) {
        char k;
        Scanner in = new Scanner(System.in);
        k = in.next().charAt(0);

        if ((k == 'a') || (k == 'i') || (k == 'u') || (k == 'e') || (k == 'o')) {
            System.out.println("HurufVokal");
        }
    }
}
