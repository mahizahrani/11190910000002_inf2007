package id.mahizahrani.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class InputJOptionPaneEX {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan:");
        
        bilangan = Integer.parseInt(box);
        
        System.out.println("Bilangan: " + bilangan);
    }
}
