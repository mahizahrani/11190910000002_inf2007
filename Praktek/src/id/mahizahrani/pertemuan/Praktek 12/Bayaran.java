package id.mahizahrani.pertemuan.keduabelas;

/**
 *
 * @author ASUS
 */
public class Bayaran {
    public int hitungBayaran(Pegawai pegawai){
        int uang = pegawai.infoGaji();
        if(pegawai instanceof Manager){
            uang += ((Manager)pegawai).infoTunjangan();
        }else if(pegawai instanceof  Programmer){
            uang +=((Programmer)pegawai).infoBonus();
        }
        return uang;
    }
    public static void main(String[] args) {
        Manager m = new Manager("Riski",300,40);
        Programmer p= new Programmer("Arkan", 500,20);
        Bayaran upah= new Bayaran();
        System.out.println("Upah Manager: " + upah.hitungBayaran(m));
        System.out.println("Upah Programmer : " + upah.hitungBayaran(p));
    }
  
}
