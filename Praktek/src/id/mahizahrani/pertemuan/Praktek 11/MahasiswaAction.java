package id.mahizahrani.pertemuan.kesebelas;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author ASUS
 */
public class MahasiswaAction {
    private final String kul = ("D:\\SequentialFile\\mhs.txt");
    public Mahasiswa KuliahMhs = new Mahasiswa();
    private String nama;
    private int nim;
    private int kodeMK;
    private int sks;
    private int nilai;
    private boolean ketemu;
    private int no;

    public String getInput(String input) {
        BufferedReader bufIn = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(input);
        System.out.flush();

        try {
            return bufIn.readLine();
        } catch (IOException e) {
            return "Error : " + e.getMessage();
        }
    }

    public void setDataMahasiswa() {

        try {
            ObjectOutputStream kul = new ObjectOutputStream(new FileOutputStream("D:\\SequentialFile\\mhs.ser"));

            Scanner in = new Scanner(System.in);
            nim = in.nextInt();
            KuliahMhs.setNim(nim);

            while (KuliahMhs.getNim() != 9999) {

                System.out.println("Masukan nama : ");
                nama = in.next();
                KuliahMhs.setNama(nama);
                System.out.println("Masukan kode Mk : ");
                kodeMK = in.nextInt();
                KuliahMhs.setKodeMK(kodeMK);
                System.out.println("Masukan SKS : ");
                sks = in.nextInt();
                KuliahMhs.setSks(sks);
                System.out.println("Masukan nilai : ");
                nilai = in.nextInt();
                KuliahMhs.setNilai(nilai);

                kul.writeObject(KuliahMhs);
                System.out.println("Masukaan NIM Anda : ");
                nim = in.nextInt();
                KuliahMhs.setNim(nim);
            }
            kul.close();
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }

    public void getDaftarNilaiNIM() {

        try {

            ObjectInputStream Kul = new ObjectInputStream(new FileInputStream("D:\\SequentialFile\\mhs.ser"));
            ketemu = false;

            Object obj = null;

            while ((obj = Kul.readObject()) != null && (!ketemu)) {
                Mahasiswa k = (Mahasiswa) obj;
                if (k.getNim() == nim) {
                    ketemu = true;
                    KuliahMhs = k;
                }
            }
            if (ketemu) {
                System.out.println("Daftar Nilai Mata Kuliah");
                System.out.println("NIM : " + KuliahMhs.getNim());
                System.out.println("Nama : " + KuliahMhs.getNama());
                System.out.println("---------------------------------------");
                System.out.println("No \tMataKuliah \t\tSKS \tNilai");
                System.out.println("---------------------------------------");
                no = 1;
                System.out.println(no + "\t" + kodeMK + "\t\t" + sks + "\t" + nilai);

                try {

                    Object o = null;
                    while ((o = Kul.readObject()) != null) {
                        Mahasiswa k2 = (Mahasiswa) o;
                        if (k2.getNim() == nim) {
                            no += 1;
                            System.out.println(no + "\t" + kodeMK + "\t\t" + sks + "\t" + nilai);

                        }
                    }
                    System.out.println("---------------------------------------");
                } catch (Exception e) {
                    System.err.println("Error : " + e.getMessage());
                }
            } else {
                System.err.println("Data Mahasiswa dengan NIM : ");
            }
            Kul.close();

        } catch (Exception e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}



