package id.mahizahrani.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author ASUS
 */
public class PengurutanSisip {

    public int[] getInsertionSert(int L[], int n) {
        int i, j, y;
        boolean ketemu;
        for (i = 2; i <= n-1; i++) {
            y = L[i];
            j = i - 1;
            ketemu = false;
            while ((j >= 0) && (!ketemu)) {
                
                    if (y < L[j]) {
                        L[j+1]=L[j];
                        j = j - 1;
                    } else {
                        ketemu = true;
                    }
            }
            L[j + 1] = y;
        }
        return L;
    }

    public static void main(String[] args) {
        int L[] = {2, 4, 3};
        int n = L.length;
        PengurutanSisip rani = new PengurutanSisip();
        System.out.println("Angka sebelum urut");
        System.out.println(Arrays.toString(L));
        rani.getInsertionSert(L, n);
        System.out.println("angka setelah urut");
        System.out.println(Arrays.toString(L));
    }
}
