package id.mahizahrani.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class LarikMultiDimensi {

    int[][] getLarikMultiDimensi(int[][] L,int []L2, int nbar, int nkol) {
        int i, j, k = 0, m, n, imin, temp;
        for (i = 0; i < nbar; i++) {
            for (j = 0; j < nkol; j++) {
                L2[k] = L[i][j];
                k++;
            }
        }

        for (i = 0; i < ((nbar * nkol) - 1); i++) {
            imin = i;

            for (j = i + 1; j < (nbar * nkol); j++) {
                if (L2[j] < L2[imin]) {
                    imin = j;
                }
            }
            temp = L2[i];
            L2[i] = L2[imin];
            L2[imin] = temp;
        }

        k = 0;
        for (i = 0; i < nbar; i++) {
            for (j = 0; j < nkol; j++) {
                L[i][j] = L2[k];
                k++;
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int i, j;
        int nbar = 0, nkol = 0;
        Scanner in = new Scanner(System.in);
        LarikMultiDimensi lov = new LarikMultiDimensi();

        System.out.print("masukkan Baris: ");
        nbar = in.nextInt();
        System.out.print("masukkan Kolom: ");
        nkol = in.nextInt();
        int[][] L = new int[nbar][nkol];
        int[] L2 = new int[nbar * nkol];

        for (i = 0; i < nbar; i++) {
            for (j = 0; j < nkol; j++) {
                System.out.print("masukkan Array[" + i + "][" + j + "} : ");
                L[i][j] = in.nextInt();
            }
        }

        System.out.println("\nArray Awal");
        for (i = 0; i < nbar; i++) {
            for (j = 0; j < nkol; j++) {
                System.out.print(L[i][j] + "  ");
            }
            System.out.println("");
        }

        lov.getLarikMultiDimensi(L, L2, nbar, nkol);
        System.out.println("\nhasil Array");
        for (i = 0; i < nbar; i++) {
            for (j = 0; j < nkol; j++) {
                System.out.print(L[i][j] + "  ");
            }
            System.out.println("");
        }
    }
}
