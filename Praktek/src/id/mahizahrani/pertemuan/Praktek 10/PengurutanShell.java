package id.mahizahrani.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author ASUS
 */
public class PengurutanShell {

    public int[] getShellShort(int L[], int n) {
        int step, start;
        int i, j, y;
        boolean ketemu;

        step = n-1;
        while (step > 1) {
            step = ((step/3) +1);
                for (start = 0; start < step; start++) {
                i = start + step;
                while (i <= n-1) {
                    y = L[i];
                    j = i - step;
                    ketemu = false;
                    while ((j >= 0) && (!ketemu)) {
                        if (y < L[j]) {
                            L[j + step] = L[j];
                            j = j - step;
                        } else {
                            ketemu = true;
                        }
                    }
                L[j + step] = y;
                i = i +step;
                
                }
            }
        }
                return L;
        }
    public static void main(String[] args) {
        int[] L = {91, 64, 31, 76, 12, 35, 97, 95, 28, 98, 41, 75, 45};
        int n = L.length;
        PengurutanShell tap = new PengurutanShell();
        
        System.out.println("Array Awal");
        System.out.println(Arrays.toString(L));
        tap.getShellShort(L,n);
        System.out.println("\nHasil Pengurutan Array");
        System.out.println(Arrays.toString(L));
    }
}

    
