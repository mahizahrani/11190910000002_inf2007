package id.mahizahrani.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Segitiga {
    double alas, tinggi, luas;
    
    public Segitiga(){
        Scanner in = new Scanner(System.in);
        System.out.println("masukkan alas:");
        alas = in.nextDouble();
        System.out.println("masukkan tinggi:");
        tinggi = in.nextDouble();
        System.out.println("masukkan luas:");
        luas = (alas * tinggi) / 2 ;
        System.out.println("Luas: " + luas);
    }
}
