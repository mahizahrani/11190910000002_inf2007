package id.mahizahrani.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class PenjumlahanDeretPecahanWhile {
        public static void main(String[] args) {
        int x;
        float s;
        
        s = 0;
        Scanner input = new Scanner(System.in);
        x = input.nextInt();
        
        while (x != -1) {            
            s = s + (float) 1 / x;
            x = input.nextInt();
        }
        System.out.println(s);
    }
    
}


