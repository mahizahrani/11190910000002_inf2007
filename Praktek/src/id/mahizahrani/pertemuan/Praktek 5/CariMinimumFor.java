package id.mahizahrani.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class CariMinimumFor {
    public static void main(String[] args) {
                int N, x, min, i;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Banyaknya data masukan, > 0 = ");
        N = in.nextInt();
        System.out.println("Data pertama = ");
        x = in.nextInt();
        min = x;
        for (i = 2; i <= N; i++) {
            x = in.nextInt();
            if (x < min) {
                min = x;
            }
        }
        System.out.println("Nilai minimum dari ketiga data diatas adalah " + min);
    }
}

    

