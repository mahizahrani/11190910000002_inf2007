package id.mahizahrani.pertemuan.kelima;

import static java.lang.Math.sqrt;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class MenuPersegiPanjanngRepeat {
    public static void main(String[] args) {
        int noMenu;
        float panjang, lebar, luas, keliling, diagonal;

        do {
            System.out.println("Menu Empat Persegi Panjang");
            System.out.println("1. Hitung Luas \n2. Hitung Keliling \n3. Hitung Panjang Diagonal \n4. Keluar Program");
            System.out.println("Masukkan pilihan anda (1/2/3/4) ?");
            Scanner in = new Scanner(System.in);
            noMenu = in.nextInt();

            switch (noMenu) {
                case 1:
                    System.out.println("Masukkan Panjang Persegi Panjang");
                    panjang = in.nextFloat();
                    System.out.println("Masukkan Lebar Persegi Panjang");
                    lebar = in.nextFloat();
                    luas = panjang * lebar;
                    System.out.println("Luasnya adalah " + luas);
                    break;
                case 2:
                    System.out.println("Masukkan Panjang Persegi Panjang");
                    panjang = in.nextFloat();
                    System.out.println("Masukkan Lebar Persegi Panjang");
                    lebar = in.nextFloat();
                    keliling = (2 * panjang) + (2 * lebar);
                    System.out.println("Kelilingnya adalah " + keliling);
                    break;
                case 3:
                    System.out.println("Masukkan Panjang Persegi Panjang");
                    panjang = in.nextFloat();
                    System.out.println("Masukkan Lebar Persegi Panjang");
                    lebar = in.nextFloat();
                    diagonal = (float) sqrt(panjang * panjang + lebar * lebar);
                    System.out.println("Diagonalnya adalah " + diagonal);
                    break;
                case 4:
                    System.out.println("Keluar program... sampai jumpa");
                    break;
            }
        } while (noMenu != 4);
    }
}


