package id.mahizahrani.pertemuan.ketiga;

/**
 *
 * @author ASUS
 */
import java.util.Scanner;
public class Selisih_Dua_Tanggal {
    public static void main(String[] args) {
        int dd1,mm1,yy1,dd2,mm2,yy2,dd3,mm3,yy3;
        int totalHari1, totalHari2, selisihHari, sisa;
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Masukan hari pertama");
        dd1 = input.nextInt(); //input hari 1
        System.out.println("Masukkan bulan pertama");
        mm1=input.nextInt(); // input bulan 1
        System.out.println("Masukkan tahun pertama");
        yy1=input.nextInt(); // input tahun 1
        System.out.println("\n\n");
        
        System.out.println("Masukan hari kedua");
        dd2 = input.nextInt(); //input hari 2
        System.out.println("Masukkan bulan kedua");
        mm2=input.nextInt(); // input bulan 2
        System.out.println("Masukkan tahun kedua");
        yy2=input.nextInt(); // input tahun 2
        
        totalHari1 = (yy1 * 365) + (mm1*30) + dd1; // untuk menghitung total hari input pertama
        totalHari2 = (yy2 * 365) + (mm2*30) + dd2; // untuk menghitung total hari input kedua
        
        selisihHari = totalHari2 - totalHari1; // selisih antara inputan pertama dan kedua
        
        yy3 = selisihHari/365; // menentukan tahun hasil selisih
        sisa = selisihHari % 365; 
        mm3 = sisa/30; // menentukan bulan hasil selisih
        dd3 = sisa % 30; // menentukan hari hasil selisih
        
        System.out.println("hasil nya   " + yy3 + ("/") + mm3 + ("/") + dd3);
    }
}
    

